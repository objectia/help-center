---
title: Documentation
layout: page
---

## Overview

* Bla 
* Bla bla
* Developer Friendly

## Install

```bash
curl https://api.stripe.com/v1/charges \
   -u sk_test_BQokikJOvBiI2HlWgH4olfQ2: \
   -d amount=999 \
   -d currency=usd \
   -d source=tok_visa \
   -d receipt_email="jenny.rosen@example.com"
```

Bla bla bla

```html
<script src="https://checkout.stripe.com/checkout.js"></script>

<script>
var handler = StripeCheckout.configure({
  key: '<%= Rails.configuration.stripe[:publishable_key] %>',
  locale: 'auto',
  name: 'Sand Castles United',
  description: 'One-time donation',
  token: function(token) {
    $('input#stripeToken').val(token.id);
    $('form').submit();
  }
});
</script>
```

## Quick Start

```xml
<dependency>
  <groupId>com.stripe</groupId>
  <artifactId>stripe-java</artifactId>
  <version>6.4.0</version>
</dependency>
```

Bla bla bla

```json
{
  "status": 200,
  "success": true,
  "test": null,
  "data": [
    { 
      "id": 1,
      "some": "more data ..."
    },
    { 
      "id": 2,
      "some": "more data ..."
    }
  ]
}
```

Bla bla bla

```html
<html>
<head>
  <title>Checkout Example</title>
</head>
<body>
<form action="/charge" method="post" class="payment">
  <article>
    <label class="amount">
      <span>Amount: $5.00</span>
    </label>
  </article>

  <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="{{ .Key }}"
    data-description="A month's subscription"
    data-amount="500"
    data-locale="auto"></script>
</form>
</body>
</html>
```

Bla bla bla

```go
package main

import (
  "github.com/objectia/support-docs"
  _ "github.com/jinzhu/gorm/dialects/sqlite"
)

type Product struct {
  gorm.Model
  Code string
  Price uint
}

func main() {
  db, err := gorm.Open("sqlite3", "test.db")
  if err != nil {
    panic("failed to connect database")
  }
  defer db.Close()

  // Migrate the schema
  db.AutoMigrate(&Product{})

  // Create
  db.Create(&Product{Code: "L1212", Price: 1000})

  // Read
  var product Product
  db.First(&product, 1) // find product with id 1
  db.First(&product, "code = ?", "L1212") // find product with code l1212

  // Update - update product's price to 2000
  db.Model(&product).Update("Price", 2000)

  // Delete - delete product
  db.Delete(&product)
}
```

Bla bla bla

```ruby
def new
end

def create
  @amount = params[:amount]

  @amount = @amount.gsub('$', '').gsub(',', '')

  begin
    @amount = Float(@amount).round(2)
  rescue
    flash[:error] = 'Charge not completed. Please enter a valid amount in USD ($).'
    redirect_to new_charge_path
    return
  end

  @amount = (@amount * 100).to_i # Must be an integer!

  if @amount < 500
    flash[:error] = 'Charge not completed. Donation amount must be at least $5.'
    redirect_to new_charge_path
    return
  end

  Stripe::Charge.create(
    :amount => @amount,
    :currency => 'usd',
    :source => params[:stripeToken],
    :description => 'Custom donation'
  )

  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_to new_charge_path
  end
end
```