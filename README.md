# Objectia Support Docs

This documentation framework is based on the hexo.io theme.
Modified to fit our www design.

## Algolia index generator setup

1. Install the [Algolia DocSearch Scraper](https://github.com/algolia/docsearch-scraper) in /usr/local

2. Create a symlink: $ ln -s /usr/local/docsearch-scraper/docsearch docsearch in /usr/local/bin

3. Create a file named .env file at the root of the project containing the following keys:
    - APPLICATION_ID=
    - API_KEY=

4. Run with: $ docsearch run docsearch.json

or

$ npm run docsearch

## License

[CCBY.40](http://creativecommons.org/licenses/by/4.0/)