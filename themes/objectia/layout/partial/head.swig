<head>
  <meta charset="utf-8">
  <title>{% if page.title %}{{ page.title }} - {% endif %}Objectia Help Center</title>
  <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!-- Canonical links -->
  <link rel="canonical" href="{{ url }}">
  
  <!-- Alternative links -->
  {% if page.layout == 'page' or page.layout == 'index' %}
    {% for lang in site.data.languages %}
      <link rel="alternative" hreflang="{{ loop.key }}" href="{{ canonical_url(loop.key) }}">
    {% endfor %}
  {% endif %}

  <!-- FAVICON -->
  <link rel="apple-touch-icon" sizes="57x57" href="https://cdn.objectia.com/assets/img/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="https://cdn.objectia.com/assets/img/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="https://cdn.objectia.com/assets/img/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="https://cdn.objectia.com/assets/img/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="https://cdn.objectia.com/assets/img/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="https://cdn.objectia.com/assets/img/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="https://cdn.objectia.com/assets/img/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="https://cdn.objectia.com/assets/img/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="https://cdn.objectia.com/assets/img/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192" href="https://cdn.objectia.com/assets/img/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="https://cdn.objectia.com/assets/img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="https://cdn.objectia.com/assets/img/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="https://cdn.objectia.com/assets/img/favicon/favicon-16x16.png">
  <link rel="manifest" href="https://cdn.objectia.com/assets/img/favicon/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="https://cdn.objectia.com/assets/img/favicon/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
  <meta name="generator" content="Hexo {{ hexo_version() }}">
  
  <!-- CSS -->
  <!-- build:css build/css/objectia.css -->
  {{ css('css/objectia') }}
  <!-- endbuild -->

  <!-- FONTS -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Mono:400,700" rel="stylesheet">
  <link href="https://cdn.objectia.com/assets/fonts/brandon-text-w01.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt"
    crossorigin="anonymous">

  <!-- UIKit -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.10/css/uikit.min.css" rel="stylesheet" type="text/css">

  <!-- SEARCH -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/docsearch.js/2.5.2/docsearch.min.css" rel="stylesheet">
  
  <!-- RSS -->
  {{ feed_tag('atom.xml') }}

  <!-- Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-123492200-2"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-123492200-2');
  </script>

  <!-- Cookie consent -->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css"
  />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
  <script>
    window.addEventListener("load", function () {
      window.cookieconsent.initialise({
        "palette": {
          "popup": {
            "background": "#1f87f0",
            "text": "#eee"
          },
          "button": {
            "background": "transparent", 
            "text": "#fff"
          }
        },
        "position": "bottom",
        "content": {
          "message": "This website uses cookies to improve your experiences. By continuing to use this website you are giving consent to cookies being used.",
          "dismiss": "OK",
          "href": "https://objectia.com/privacy"
        }
      })
    });
  </script>


  <!-- Zendesk Widget -->
  <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=0c84088f-e0b6-4acb-ac00-68b8ebc48143"></script>
  <script type="text/javascript">
    window.zESettings = {
      webWidget: {
        zIndex: 10000,
        color: {
          launcher: '#1f87f0',
          launcherText: '#fff'
        }      
      }
    };
  </script>


  <!-- Search -->
  <script type="text/JavaScript">
    function onSearchKeyDown(e) {
      e = e || window.event;
      if (e.key == "Escape") {
        e.preventDefault();
        toggleSearch(true);
      }
    }

    function toggleSearch(forceHide) {
      var i;
      var navLinks = document.getElementsByClassName("main-nav-link");
      var icon = document.getElementById("search-icon");
      var input = document.getElementById("search-input")
      if (forceHide || input.style.display === "inline-block") {
          for (i = 0; i < navLinks.length; i++) {
            navLinks[i].style.display = "inline-block";
          }
          icon.className = "far fa-search";
          input.value = "";
          input.style.display = "none";
      } else {
          for (i = 0; i < navLinks.length; i++) {
            navLinks[i].style.display = "none";
          }
          icon.className = "fas fa-times";
          input.value = "";
          input.style.display = "inline-block";
          input.focus();
      }
    }

    function onWindowResize(e) {
      e = e || window.event;
      toggleSearch(true);
    }
    window.addEventListener("resize", onWindowResize);
  </script>
</head>
